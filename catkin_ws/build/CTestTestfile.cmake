# CMake generated Testfile for 
# Source directory: /home/vladpavluts/catkin_ws/src
# Build directory: /home/vladpavluts/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("beginner/robocake_algorithm")
subdirs("beginner/robocake_controllers")
subdirs("beginner/robocake_description")
subdirs("beginner/robocake_gazebo")
