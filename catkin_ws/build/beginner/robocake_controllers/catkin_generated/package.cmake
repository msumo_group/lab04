set(_CATKIN_CURRENT_PACKAGE "robocake_controllers")
set(robocake_controllers_VERSION "0.0.0")
set(robocake_controllers_MAINTAINER "olegdiominov <olegdiominov@todo.todo>")
set(robocake_controllers_PACKAGE_FORMAT "2")
set(robocake_controllers_BUILD_DEPENDS )
set(robocake_controllers_BUILD_EXPORT_DEPENDS )
set(robocake_controllers_BUILDTOOL_DEPENDS "catkin")
set(robocake_controllers_BUILDTOOL_EXPORT_DEPENDS )
set(robocake_controllers_EXEC_DEPENDS )
set(robocake_controllers_RUN_DEPENDS )
set(robocake_controllers_TEST_DEPENDS )
set(robocake_controllers_DOC_DEPENDS )
set(robocake_controllers_URL_WEBSITE "")
set(robocake_controllers_URL_BUGTRACKER "")
set(robocake_controllers_URL_REPOSITORY "")
set(robocake_controllers_DEPRECATED "")